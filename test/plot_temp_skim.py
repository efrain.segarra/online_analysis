import matplotlib; matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np

times = []
temps9 = []
temps10 = []
with open("temp_skim.txt","r") as f:
    for line in f:
        parse = line.strip().split()
        if parse:
            temps9.append(float(parse[9]))
            temps10.append(float(parse[10]))
            times.append(float(parse[0]))


plt.scatter(times,temps9,linestyle='--',color='red',marker='o',label='temp tank upper mapper flange T09')
plt.scatter(times,temps10,linestyle='--',color='blue',marker='o',label='temp tank front lid bottom T06')
plt.ylim([21,22])
plt.ylabel('Temperature (C)',fontsize=17)
plt.xlabel('Time after start of cycle (s)',fontsize=17)
plt.legend(numpoints=1,loc='best')
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.tight_layout()
plt.savefig('temp_skim.pdf',bbox_inches='tight')
plt.show()
