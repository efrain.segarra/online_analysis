#include <iostream>
#include <fstream>
#include <cstring>
#include "SimpleLog.h"
#include "N2readData.h"
#include <vector>
#include <map>


using namespace std;

int main( int argc, char **argv ){

	if( argc != 2 ){
		cerr << "exiting\n";
		return -1;
	}

	SimpleLog_Setup(NULL, NULL, 0, 0, 0, "\t");
	SimpleLog_FilterLevel(SL_ERROR|SL_WARNING /*|SL_NOTICE SL_ALL*/); // Default is SL_ALL

	tN2data N2data = {0};
	N2_ReadFile(argv[1], &N2data);

	
	int NbCol = N2data.NbCol;
	int NbRow = N2data.NbRow;

	const int time_sample = 10; // every 10 seconds, store values
	std::map<int,std::vector<double>> values_d;
	std::map<int,std::vector<uint64_t>> values_u;
	uint64_t last_time = 0;
	

	uint64_t start_time = (long long)N2data.FirstTimeStamp;
	if( N2data.Data && N2data.NbRow > 0 && N2data.ReservedSize > 0 ){
		for( int r=0; r<N2data.NbRow ; r++ ){
			uint64_t time = (((long long *)N2data.TimeStamp)[r] - start_time ) / 1E9;
			// Sampling the values
			if( last_time + time_sample <= time ){
				last_time = time;
				
				cout << time << " ";
				for( int c=0; c<N2data.NbCol ; c++ ){


					if( 0==strcmp(N2data.Columns[c].DataType,"double")){
						double value= ((double**)N2data.Data)[r][c];
						cout << value << " ";
					}
					else{
						uint64_t value = ((long long **)N2data.Data)[r][c];
						cout << value << " ";
					}
				}
			}

			cout << "\n";
		}

	}



	return 1;
}
