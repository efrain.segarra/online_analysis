#include <iostream>
#include <string.h>
#include <unistd.h>	// close(socket)
#include <netdb.h>	// NI_MAXHOST
#include <arpa/inet.h> 	// inet_pton
//#include <stdlib.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <stdio.h>
#include <fstream>
#include <cstring>
#include "SimpleLog.h"
#include "N2readData.h"
#include <vector>
#include <map>

using std::cout;
using std::cerr;
using std::to_string;
using std::string;
using std::cin;

int create_tcp_client(const char* ip_address, const int port );
int send_data(const int client_socket, const char* data );

int main( int argc, char** argv ){

	if( argc != 4 ){
		cerr << "Invalid number of arguments. Instead use:\n";
		cerr << "\t./client [ip address] [port] [data file]\n";
	}
	
	////////////////////////////////////////////////////
	// Open up client socket to send data
	char* ip_address = argv[1];
	int port = atoi(argv[2]);
	int client_socket = create_tcp_client(ip_address,port);



	SimpleLog_Setup(NULL, NULL, 0, 0, 0, "\t");
	SimpleLog_FilterLevel(SL_ERROR|SL_WARNING /*|SL_NOTICE SL_ALL*/); // Default is SL_ALL
									  
	tN2data N2data = {0};
	N2_ReadFile(argv[3], &N2data);

	
	double max_temp = -1e5;
	double min_temp = 1e5;

	if( N2data.Data && N2data.NbRow > 0 && N2data.ReservedSize > 0 ){
		for( int r=0; r<N2data.NbRow ; r++ ){
			for( int c=0; c<N2data.NbCol ; c++ ){
				if( c != 9 ) continue;

				if( 0==strcmp(N2data.Columns[c].DataType,"double")){
					double value= ((double**)N2data.Data)[r][c];
					if( value > max_temp ) max_temp = value;
					if( value < min_temp ) min_temp = value;
				}
				else{
					uint64_t value = ((long long **)N2data.Data)[r][c];
					if( value > max_temp ) max_temp = value;
					if( value < min_temp ) min_temp = value;
				}
			}
		}
	}
	cout << "found max/min temps: " << max_temp << " " << min_temp << "\n";
	
	////////////////////////////////////////////////////
	// Send data to server
	//while( true ){
		
	string input_str = to_string(max_temp) + " " + to_string(min_temp);

	int status = send_data( client_socket, input_str.c_str() );
	if( status != 1 ){
		cerr << "Some error in data transmission. Exiting...\n";
		return -1;
	}

	//}


	
	////////////////////////////////////////////////////
	// closing the client socket
	close(client_socket);
	return 0;
}


int create_tcp_client(const char* ip_address, const int port ){
	////////////////////////////////////////////////////
	// Grab ip address and port from arguments
	string destination(ip_address);
	destination += ":";
	destination += to_string(port);


	////////////////////////////////////////////////////
	// Create an AF_INET stream socket for the client:
	int client_socket = socket(AF_INET, SOCK_STREAM, 0);
	if( client_socket < 0 ){
		cerr << "Unable to create TCP socket! Exiting...\n";
		close(client_socket);
		return -1;
	}


	////////////////////////////////////////////////////
	// Setup server socket to connect to
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);		// host-to-network-short 
	int ip_check = inet_pton( AF_INET, ip_address, &server_addr.sin_addr ); // convert ip to binary
	if( ip_check <= 0 ){
		cerr << "Invalid address or not supported. Exiting...\n";
		return -1;
	}


	////////////////////////////////////////////////////
	// Connect to server
	int client_connect = connect( client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr) );
	if( client_connect < 0 ){
		cerr << "Connection to " << destination << " failed! Exiting...\n";
		return -1;
	}

	return client_socket;
}

int send_data(const int client_socket, const char* data ){
	char buffer[4096];

	if( sizeof(data) > 4096 ){
		cerr << "Cannot send data of this size to server. Exiting...\n";
		return -1;
	}

	cout << "Client sending to server:\n\t" << data << "\n";
	int bytes_sent = send( client_socket, data, strlen(data), 0 );
	if( bytes_sent <= 0 ){
		cerr << "Error in send(). Exiting...\n";
		return -1;
	}

	memset(buffer,0,4096);

	int bytes_received = recv( client_socket, buffer, 4096, 0);
	if( bytes_received <= 0){
		cerr << "Error in recv() from server. Exiting...\n";
		return -1;
	}
	cout << "Client received back from server:\n\t" << buffer << "\n";

	if( bytes_received-1 == bytes_sent ){
		cout << "Successful transmission. Waiting for next data packet...\n";
		return 1;
	}
	else{
		cout << "Some error in transmission. Exiting...\n";
		return -1;
	}
}
