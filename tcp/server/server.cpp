#include <iostream>
#include <string.h>
#include <unistd.h>	// close(socket)
#include <netdb.h>	// NI_MAXHOST
#include <arpa/inet.h> // inet_addr
//#include <stdlib.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <stdio.h>

using std::cout;
using std::cerr;
using std::to_string;
using std::string;

int main( int argc, char** argv ){

	if( argc != 2 ){
		cerr << "Invalid number of arguments. Instead use:\n";
		cerr << "\t./server [port]\n";
	}
	
	////////////////////////////////////////////////////
	// Grab port from arguments
	int port = atoi(argv[1]);
	
	
	////////////////////////////////////////////////////
	// Create an AF_INET stream socket for the server:
	int listening_socket = socket(AF_INET, SOCK_STREAM, 0);
	if( listening_socket < 0 ){
		cerr << "Unable to create TCP socket! Exiting...\n";
		close(listening_socket);
		return -1;
	}

	////////////////////////////////////////////////////
	// Bind the ip address and port to a socket
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);		// host-to-network-short 
	addr.sin_addr.s_addr = INADDR_ANY; 	// bind to any address 
	
	// Set socket options:
	int opt = 1;
	int set_socket_opts = setsockopt( listening_socket, 
			SOL_SOCKET, // set options at the socket level
			SO_REUSEADDR | SO_REUSEPORT, // options to set: allows for reuse in local addresses & ports in BIND
			&opt, sizeof(opt) ); // setting value (1==true, 0==false)
	if( set_socket_opts < 0 ){
		cerr << "Unable to set socket options\n";
		close(listening_socket);
		return -1;
	}

	int connection_status = bind( listening_socket, (struct sockaddr *)&addr, sizeof(addr) );
	if( connection_status < 0 ){
		cerr << "Unable to connect to port!\n";
		close(listening_socket);
		return -1;
	}

	////////////////////////////////////////////////////
	// Connect this socket for listening:
	int listening_status = listen( listening_socket, SOMAXCONN);
	if( listening_status < 0 ){
		cerr << "Unable to start listening\n"; 
		close(listening_socket);
		return -1;
	}

	////////////////////////////////////////////////////
	// Wait for a client connection
	sockaddr_in client_addr;
	int client_size = sizeof(client_addr);
	
	int client_socket = accept( listening_socket, (struct sockaddr*)&client_addr, (socklen_t*)&client_size );
	if( client_socket < 0 ){
		cerr << "Invalid client connection\n";
		close(listening_socket);
		return -1;
	}
	// If we have valid socket, print out client information
	char host[NI_MAXHOST]; 		// Client's remote name
	char service[NI_MAXSERV];	// service (port) the client is connected on
	memset(host, 0, NI_MAXHOST);
	memset(service, 0, NI_MAXSERV);

	// Try to get the hostname client
	//if( getnameinfo( (struct sockaddr*)&client_addr, client_size, host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0 ){
	if( false ){
		cout << host << " is connected on port " << service << "\n";
	}
	else{
		inet_ntop(AF_INET, &client_addr.sin_addr, host, NI_MAXHOST );
		cout << host << " connected on port " << ntohs(client_addr.sin_port) << "\n";
	}

	////////////////////////////////////////////////////
	// Close the listening socket if it's not needed
	close(listening_socket);


	////////////////////////////////////////////////////
	// Read in the data and echo message back to client
	char buffer[4096];

	while( true ){
		memset(buffer, 0, 4096);
	
		// Wait for client to send data
		int bytes_received = recv( client_socket, buffer, 4096, 0 );
		if( bytes_received < 0 ){
			cerr << "Error in recv(). Exiting...\n";
			return -1;
		}
		if( bytes_received == 0 ){
			cerr << "Client disconnected.\n";
			return -1;
		}
		
		// 
		cout << "Server recevied:\n\t" << buffer << "\n";

		// Echo message back to client
		send( client_socket, buffer, bytes_received + 1, 0 );
		
	}
	

	////////////////////////////////////////////////////
	// Close and shutdown TCP server
	close(client_socket);
	shutdown(listening_socket, SHUT_RDWR);


	return 1;
}
